import machine
from machine import Pin,ADC
import utime

#Select ADC input 0 (GPIO26)
ADC_ConvertedValue = machine.ADC(machine.Pin(36))
#DIN = Pin(22,Pin.IN)
conversion_factor = 3.3 / (65535)

def gas():
    AD_value = ADC_ConvertedValue.read_u16() * conversion_factor
    return AD_value
    #utime.sleep(0.5)


