from machine import Pin, I2C
from time import sleep
ledred = Pin(17, Pin.OUT)
ledblue = Pin(16, Pin.OUT)
ledgreen = Pin(23, Pin.OUT)
for i in range (3):
    ledred.value(0)
    sleep(0.5)
    ledred.value(1)
    sleep(0.5)
    
ledred.value(0)
print('off red')
ledgreen.value(0)
print('off green')
ledblue.value(0)
print('off blue')
