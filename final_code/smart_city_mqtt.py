#sends data via mqqtt to RPI nodered
import json
import dht
import dust_sensor as d
import random
import time
from umqttsimple import MQTTClient
import ubinascii
import machine
import micropython
import network
import esp
from machine import Pin, SoftI2C, ADC
esp.osdebug(None)
import gc
gc.collect()
import dust_sensor as d
import gas_sensor as g
import ldr as l

#variables
sensor = dht.DHT11(Pin(15))

ssid = 'Cheska_Laptop_hotspot'
password = 'Hello_world'
mqtt_server = '192.168.137.49'

client_id = ubinascii.hexlify(machine.unique_id())

#create topics
topic_pub_temp = b'esp/dht11/temp'
topic_pub_humid = b'esp/dht11/humid'
topic_pub_gas = b'esp/gas_sensor'
topic_pub_dust = b'esp/dust_sensor'
topic_pub_ldr = b'esp/ldr_value'
topic_json_data = b'json_data'


last_message = 0
message_interval = 5

station = network.WLAN(network.STA_IF)

station.active(True)
#station.connect(ssid, password)

while station.isconnected() == False:
    pass

print('Connection successful')

def connect_mqtt():
    global client_id, mqtt_server
    client = MQTTClient(client_id, mqtt_server, keepalive=30)
    client.connect()
    print('Connected to %s MQTT broker' % (mqtt_server))
    return client

def restart_and_reconnect():
    print('Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(10)
    machine.reset()

try:
    client = connect_mqtt()
except OSError as e:
    restart_and_reconnect()


def createPayload(ldr, temp, hum, dust, gas):
    pl = "{'Light':%s, 'Temperature':%s, 'Humidity':%s, 'Dust':%s, 'Gas':%s}" %(ldr, temp, hum, dust, gas)
    pl = json.dumps(pl)
    return pl

while True:
    try:
        if (time.time() - last_message) > message_interval:
            #Sensor 1
            sensor.measure()
            temp = sensor.temperature()
            hum = sensor.humidity()
            gas = g.read_gas()
            dust = d.monitor()
            ldr = l.read_ldr()
            json_sens = createPayload(ldr,temp,hum,dust,gas)
            
            client.publish(topic_pub_temp, str(temp))
            client.publish(topic_pub_humid, str(hum))
            client.publish(topic_pub_gas, str(gas))
            client.publish(topic_pub_dust, str(dust))
            client.publish(topic_pub_ldr, str(ldr))
            client.publish(topic_json_data, json_sens)
            last_message = time.time()
            
    except OSError as e:
        restart_and_reconnect()

