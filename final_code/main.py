# final! main file sending data to the LCD
 
import machine
from machine import Pin, SoftI2C, ADC, I2C
from lcd_api import LcdApi
from i2c_lcd import I2cLcd
from time import sleep
import dust_sensor as d
import gas_sensor as g
import ldr as l
import dht
#import save_data as sql

sensor = dht.DHT11(Pin(15))
I2C_ADDR = 0x27
totalRows = 2
totalColumns = 16
i2c = SoftI2C(scl=Pin(22), sda=Pin(21), freq=10000)     #initializing the I2C method for ESP32
lcd = I2cLcd(i2c, I2C_ADDR, totalRows, totalColumns)
   
    
def init_rgb():
    global ledred
    global ledblue
    global ledgreen
    ledred = Pin(17, Pin.OUT)
    ledgreen = Pin(23, Pin.OUT)
    ledblue = Pin(16, Pin.OUT)

    
def led_val(a):
    #a = led.value() #led color
    for i in range (3):
            a.value(0)
            sleep(0.2)
            a.value(1)
            sleep(0.2)
    

def rgb_off(r,g,b):
    #print('rgb off!')
    r.value(0)
    g.value(0)
    b.value(0)
    sleep(0.5)
    
def max_temp(temp):
    if temp >= 25:
        led_val(ledgreen)
        print('Warm! ' + str(temp) + 'C')
    elif temp < 15:
        print('Cold')
        led_val(ledblue)
    else:
        print('The temperature is perfect !  ' + str(temp) + 'C')

        
        
def max_humid(hum):
    if hum >= 50:
        led_val(ledred)
    else:
        rgb_off(ledred,ledgreen,ledblue)
        print('The air is perfect! '+ str(hum) + '%')
        


def dht_value(t,h):
    lcd.putstr('Temp value:' + '\n')
    lcd.putstr(str(t)+ ' C')
    max_temp(temp)
    sleep(2)
    lcd.clear()
    lcd.putstr('Humid value:' + '\n')
    lcd.putstr(str(h) + ' %')
    max_humid(hum)
    sleep(2)
    lcd.clear()
    
    #print('Temperature: %3.1f C' %temp)
    #print('Humidity: %3.1f %%' %hum)
    
def dust_value(dust):
    lcd.putstr('Dust amount:' + '\n')
    lcd.putstr(str(dust)+ ' ug/m3 ')
    ledblue.value(1)
    sleep(2)
    lcd.clear()

def gas_value(gas):
    lcd.putstr('Air quality:' + '\n')
    lcd.putstr(str(gas)+ ' ppm')
    led_val(ledgreen)
    sleep(2)
    lcd.clear()

def ldr_value(light):
    lcd.putstr('Brightness:' + '\n')
    lcd.putstr(str(light) + ' level')
    led_val(ledred)
    sleep(2)
    lcd.clear()
    
def begin_prog():
    lcd.putstr('Welcome to' + '\n')
    lcd.putstr('Smart City')
    print("Smart city activated") 
    sleep(2)
    lcd.clear()


init_rgb()
rgb_off(ledred,ledgreen,ledblue)
begin_prog()
while True:
    try:
        sleep(2)
        sensor.measure()
        temp = sensor.temperature()
        hum = sensor.humidity()        
        #sql.save(tem, hum, dust, air)  # sending real time data to file data_sql.py
        dht_value(temp, hum)
        dust = d.monitor()
        dust_value(dust)
        air = g.read_gas()
        gas_value(air)
        b = l.ldr()
        ldr_value(b)
        sleep(2)
        rgb_off(ledred,ledgreen,ledblue)
        
    except OSError as e:
        print('Failed to read sensor.')

