### Embedded Systems Week 18

## Exercise 0 & 1 - Dust Sensor

# Wires
S-GND 		: Signal Ground
Vo 			: Voltage Output
V-LED		: Voltage LED

# Wiring Documentation
Fritzing	: https://gitlab.com/group4_project/embedded_project/-/blob/main/smart_city_sensor-fritzing.png

# Programming it
Dust Sensor : https://github.com/AllanMisasa/LectureCode/blob/main/Programming%20and%20Embedded/sharp_gp2y_dust.py 
(Allan's Code)

MQ-135		: https://gitlab.com/group4_project/embedded_project/-/blob/main/MQ135.py


# Dust Sensor Wiring
| Color       | Description     |
| ----------- | ----------------|
| Blue Wire   | Voltage LED     |
| Green Wire  | LED Ground      |
| White Wire  | LED PIN         |
| Yellow Wire | Source Ground   |
| Black Wire  | V-out           |
| Red Wire    | Vcc (5V)        |

# Gas Sensor Wiring
| Color       | Description     |
| ----------- | ----------------|
| Red Wire    | 3V 	            |
| Black Wire  | Ground          |
| Green Wire  | Digital Pin(22) |
| Blue Wire   | Analog Pin(36)  |
