# Week 11
## Sensors for rover IoT expansion project.

### Sensor 1

Name: BME280.  
Values : Temperature, Humidity, Pressure.  
Datasheet : https://www.mouser.com/datasheet/2/783/BST-BME280-DS002-1509607.pdf

#### Alternatives

Name: DHT11  
Values: Temperature, Humidity.  
Datasheet: https://www.mouser.com/datasheet/2/758/DHT11-Technical-Data-Sheet-Translated-Version-1143054.pdf

### Sensor 2

Name: MQ-135  
Values: Gas Sensor, Air Quality  
Datasheet : https://components101.com/sites/default/files/component_datasheet/MQ135%20Datasheet.pdf  
3D Case: https://www.thingiverse.com/thing:2850597 

#### Alternatives

Name: MG811  
Values: Carbon Dioxide (Co2)  
Datasheet: https://sandboxelectronics.com/files/SEN-000007/MG811.pdf  
Loads of air quality sensors. (https://www.mysensors.org/build/gas)

Name: BME680  
Values: Pressure, Gas.  
Datasheet: https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bme680-ds001.pdf

### Sensor 3

Name: Sharp Dust Sensor (GP2Y1010AU0)  
Values: House Dust, Cigarette smoke.  
Datasheet: https://global.sharp/products/device/lineup/data/pdf/datasheet/gp2y1010au_appl_e.pdf  
3D Case: https://www.thingiverse.com/thing:4518420

#### Alternatives

Name: Grove Dust Sensor (PPD42NS)  
Values: Air Particles.  
Datasheet: https://files.seeedstudio.com/wiki/Grove_Dust_Sensor/resource/Grove_-_Dust_sensor.pdf